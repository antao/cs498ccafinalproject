# Uncomment this line when running in Zeppelin notebooks
#%spark2.pyspark

# Import the libraries we will need
import time
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count
from pyspark.sql.functions import avg
from pyspark.sql.functions import round
from pyspark.sql.functions import cume_dist
from pyspark.sql.window import Window

# Configure the authentication to our S3 instance
sc._jsc.hadoopConfiguration().set('fs.s3n.awsAccessKeyId', 'AKIAJYYXUAARX5BFPXRQ')
sc._jsc.hadoopConfiguration().set('fs.s3n.awsSecretAccessKey', '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')

# Load the data from our S3 instance
masterData = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Master.csv')

# Display the schema for both files
masterData.printSchema()

# Start the benchmark timer
startTime = time.time()
print 'Starting benchmark at: {}'.format(startTime)

# Query the birth state and count for all US born players
questionData = masterData.createOrReplaceTempView('masterData')

# Generate our query
sqlDF = spark.sql('select birthState, count(birthState) as myCount from masterData where birthCountry = "USA" group by birthState order by birthState asc')

# Display results
sqlDF.show()

# Export to CSV
sqlDF.write.csv('/tmp/Question6.csv')

# End the benchmark timer
endTime = time.time()
print 'Ending benchmark at: {}'.format(endTime)
totalTime = endTime - startTime
print 'Total processing time: {}'.format(totalTime)