# Uncomment the line below when running in Zeppelin notebooks
#%spark2.pyspark

# Import the libraries we will need
import time
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count
from pyspark.sql.functions import avg
from pyspark.sql.functions import round
from pyspark.sql.functions import cume_dist
from pyspark.sql.window import Window

# Configure the authentication to our S3 instance
sc._jsc.hadoopConfiguration().set('fs.s3n.awsAccessKeyId', 'AKIAJYYXUAARX5BFPXRQ')
sc._jsc.hadoopConfiguration().set('fs.s3n.awsSecretAccessKey', '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')

# Load the data from our S3 instance
masterData = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Master.csv')
battingData = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Batting.csv')

# Display the schema for both files
masterData.printSchema()
battingData.printSchema()

# Start the benchmark timer
startTime = time.time()
print 'Starting benchmark at: {}'.format(startTime)

# Merge the two data frames
questionData = battingData.join(masterData, masterData.playerID == battingData.playerID, 'left')

# Calculate the players age (Using the year only since we aren't calculating the value for every single game and it is summarized in the playing year)
questionData = questionData.withColumn('playerAge', questionData.yearID - questionData.birthYear)

# Filter out lines where the players age could not be calculated (likely because the birth year was blank)
questionData = questionData.na.drop(subset=["playerAge"])

# Filter out players who didn't have at least 502 batting chances which qualifies them for a batting title
questionData = questionData.filter(questionData.AB >= 502)

# Remove outliers - in this case we are going to remove any player 50 years or older
questionData = questionData.filter(questionData.playerAge < 50)

# Focus on players over 30 years of age
steroidEra = questionData.filter(questionData.playerAge >= 30)

# Query the averge batting value for players by year
steroidEra = steroidEra.createOrReplaceTempView('steroidEra')

sqlDF = spark.sql('select yearID, avg(HR) from steroidEra group by yearID order by yearID asc')
sqlDF.show()

# Export to CSV
sqlDF.write.csv('/tmp/Question3b.csv')

# End the benchmark timer
endTime = time.time()
print 'Ending benchmark at: {}'.format(endTime)
totalTime = endTime - startTime
print 'Total processing time: {}'.format(totalTime)