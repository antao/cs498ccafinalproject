#-------------------------------------------------------------------------------
# Name:        Q2_Relation_Salary_Wins
# Purpose: Explore the Relationship of Team Salaries to Wins
#
# Author:      antaonn
#
# Created:     16/04/2018
# Copyright:   (c) antaonn 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import time
starttime = time.time()
import os.path
import pandas as pd


# This is only needed for the Local Folder Structure in the project
my_path = os.path.abspath(os.path.dirname(__file__))

salarypath = os.path.join(my_path, "../data/Salaries.csv")
teampath = os.path.join(my_path, "../data/Teams.csv")

# Create Data Frames for Salary Table and Team Table
df_sal = pd.read_csv(salarypath)
df_team = pd.read_csv(teampath)

# Sum up salaries of all the players, for each team, for each year from the Salaries.csv file
yearly_salary = df_sal.groupby(['yearID', 'teamID'], as_index=False).sum()
# print yearly_salary

# Filter the Year, Team and Win Data from Teams.csv file
df_team_filt = df_team.filter(items=['yearID', 'teamID', 'W'])
yearly_wins = df_team_filt.groupby(['yearID', 'teamID'],  as_index=False).max()
#print yearly_wins

# Merge the Yearly Wins with the Yearly Salaries
merge =  yearly_salary.merge(yearly_wins)
merge.to_csv("Team_Salary_Versus_Wins.csv")
#print merge
print (time.time()-starttime)