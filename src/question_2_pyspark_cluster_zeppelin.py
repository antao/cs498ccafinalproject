# Uncomment this line when running on Zeppelin.
#%spark2.pyspark

import time

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count
from pyspark.sql.functions import sum
from pyspark.sql.functions import rank
from pyspark.sql.functions import avg

from pyspark.sql.window import Window

# We create a sql context object, so that we can read in csv files easily, and create a data frame
sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", "AKIAJYYXUAARX5BFPXRQ")
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", "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")

df_salary =sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Salaries.csv')
df_team = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Teams.csv')

starttime = time.time()

keep = [df_salary.yearID, df_salary.teamID, df_salary.salary ]
df_salary_filt = df_salary.select(*keep)

df_salary_agg_raw = df_salary_filt.groupBy(df_salary_filt.yearID, df_salary_filt.teamID).\
    agg(sum(df_salary_filt.salary)).\
    orderBy(df_salary_filt.yearID, df_salary_filt.teamID)

df_salary_agg = df_salary_agg_raw.withColumnRenamed('yearID', 'year').withColumnRenamed('teamID', 'team')


# From the Team table select the Wins, Divisonal Rank, Division Champion state, Wild Card Winner state, League Champion state,
# World Series Champion state, Runs, Hits, Home Runs, Runs Allowed, Earned Run Average, Hits Allowed and Home Runs Alowed

keep2 = [df_team.yearID, df_team.teamID, df_team.W, df_team.DivWin, df_team.WCWin, df_team.LgWin, df_team.WSWin, df_team.R,\
         df_team.H, df_team.HR, df_team.RA, df_team.ERA, df_team.HA, df_team.HRA]

df_team_filt = df_team.select(*keep2)

cond = [df_team_filt.yearID == df_salary_agg.year, df_team_filt.teamID == df_salary_agg.team]

# Join the Team Table and the Salart Table

df_merge_raw = df_team_filt.join(df_salary_agg, cond, 'inner')

df_merge = df_merge_raw.withColumnRenamed('sum(salary)', 'salary')

#df_merge.show()



# #### Pyspark Additional Statistics
# To put our data into context, we can also look up the following information
#
# 1) Extract the Highest spending teams each year (1984 and later), and examine their performance
#
#     a) Find the Number and Percentage of times the Top Spending Team has won the World Series
#     b) Find the Number and Percentage of times the Top Spending Team has won their League (AL/NL)
#     c) Find the Number and Percentage of times the Top Spending Team has won their Division
#     d) Find the Number and Percentage of times the Top Spending Team has made the Playoffs
#
# 2) Extract the World Series winning team each year, and examine their salary rank
#
#     a) Find the Number and Percentage of times the Word Series Winner is in the Top 5 spenders in the league
#     b) Find the Number and Percentage of times the Word Series Winner is in the Top 10 spenders in the league
#
#

# In[6]:

# Rank each team by the amount of money spent every year

windowSpec = Window.partitionBy(df_merge['yearID']).orderBy(df_merge['salary'].desc())
df_big_spender = df_merge.withColumn("yearRank", rank().over(windowSpec))




# In[7]:

# Extract the Top Spending Team Every Year, and look at their performance

df_top_spender = df_big_spender.filter(df_big_spender.yearRank == 1).orderBy(df_big_spender['yearID'], ascending=False)
#df_top_spender.show()


# In[8]:

# Number of time the Top Spending Team has won the world series after 1984
# Ans: 5

#topSpendWs = df_top_spender.filter(df_top_spender.WSWin=="Y").count()
#teamCount = df_top_spender.count()

#print (topSpendWs)


# In[9]:

# Percentage of time the top spending team has won the world series after 1984
# Ans: 15.635 %

#print ((topSpendWs/teamCount)*100)


# In[10]:

# Number of times the top spending team has won their League (AL/NL) after 1984
# Ans: 7

#topSpendLg = df_top_spender.filter(df_top_spender.LgWin=="Y").count()
#print (topSpendLg)


# In[11]:

# Percentage of time the top spending team has won their League (AL/NL) after 1984
# Ans: 21.875 %

#print ((topSpendLg/teamCount)*100)


# In[12]:

# Number of times the top spending team has won their Division after 1984
# Ans: 15

#topSpendDiv = df_top_spender.filter(df_top_spender.DivWin=="Y").count()
#print (topSpendDiv)


# In[13]:

# Percentage of time the top spending team has won their Division after 1984
# Ans: 46.875%

#print ((topSpendDiv/teamCount)*100)


# In[14]:

# Number of times the top spending team has made it to the Playoffs
# Ans: 19

#topSpendPl = df_top_spender.filter("DivWin=='Y' OR WCWin=='Y'").count()


# In[15]:

# Percentage of times the top spending team has made it to the Playoffs
# Ans: 59.375%

#print((topSpendPl/teamCount)*100)


# In[16]:

# Extract the Word Series Team Every Year, and look at their Spending Rank

df_ws_winner = df_big_spender.filter(df_big_spender.WSWin == "Y").orderBy(df_big_spender['yearID'], ascending=False)
#df_ws_winner.show()



# In[17]:

# Number of times the World Series Winner is in the Top 5 spenders in the League (After 1984)
# Ans: 14

#wsTop5 = df_ws_winner.filter(df_ws_winner.yearRank <=5).count()
#teamCount = df_ws_winner.count()
#print (wsTop5)


# In[18]:

# Percentage of times the World Series Winner is in the Top 5 spenders in the League (After 1984)
# Ans: 45.16%

#print ((wsTop5/teamCount)*100)


# In[19]:

# Number of times the World Series Winner is in the Top 10 spenders in the League (After 1984)
# Ans: 21

#dvTop5 = df_ws_winner.filter(df_ws_winner.yearRank <=10).count()
#print (dvTop5)


# In[20]:

# Percentage of times the World Series Winner is in the Top 10 spenders in the League (After 1984)
# Ans: 67.74%

#print ((dvTop5/teamCount)*100)


# #### Calculate the Average Number of Wins and Average Salary of the Top Spending Teams
# We can partition the data by a Teams spending rank, and look at the Average number of wins achieved by each team based on
# their spending rank. We can also look at their Average amount of Salary expenditure. From the Data we see that the Teams
# that spend the most on average, also win the most on average. The Highest spending teams since 1984 have averaged 89 wins
# per season. They have also roughly spent an average of $118 million on salary.
#
# There is a large gap between the Average spending of the Top team, and the Average spending of the Team that spends the
# fifth most amount of money. There is roughly a 50 percent increase in average spending between these teams. However there
# is only an average difference of 5 wins per season between these teams.
#

# In[21]:

# Calculate the Average number of wins by Spending Rank

windowSpec2 = Window.partitionBy(df_big_spender['yearRank']).orderBy(df_big_spender['yearRank'].desc())
df_avg_win = df_big_spender.withColumn("avgWin", avg(df_big_spender['W']).over(windowSpec2)).\
    withColumn("avgSal", 0.000001*avg(df_big_spender['salary']).over(windowSpec2))


df_avg_win_fin = df_avg_win.filter(df_avg_win.yearID == 2016).orderBy(df_avg_win['yearRank'])

keep4 = [df_avg_win_fin.yearRank, df_avg_win_fin.avgWin, df_avg_win_fin.avgSal]
df_avg_win_fin_filt = df_avg_win_fin.select(*keep4)

#df_avg_win_fin_filt.show()



# #### Pyspark Test Results
# We convert our spark data frames to pandas data frames, so it is easy to save them in a human readable csv format. These
# files contain the answers to the questions we posed.

# In[22]:

# Examples to show how to print the results to an output file

keep3 = [df_merge.yearID, df_merge.teamID, df_merge.W, df_merge.salary]
df_merge_wins_sal = df_merge.select(*keep3).filter(df_merge.yearID>2006)

pandas_merge_wins_sal = df_merge_wins_sal.toPandas()

pandas_merge = df_merge.toPandas()
pandas_top_spender = df_top_spender.toPandas()
pandas_ws_winner = df_ws_winner.toPandas()
pandas_avg_sal_wins = df_avg_win_fin_filt.toPandas()

print (time.time()-starttime)
