# Uncomment the line below when running in Zeppelin notebooks
#%spark2.pyspark

import time

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count
from pyspark.sql.functions import avg
from pyspark.sql.functions import round
from pyspark.sql.functions import cume_dist


from pyspark.sql.window import Window


# We create a sql context object, so that we can read in csv files easily, and create a data frame
sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", "AKIAJYYXUAARX5BFPXRQ")
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", "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")

df_master =sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Master.csv')
df_bat = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Batting.csv')

starttime = time.time()

keep = [df_master.playerID, df_master.birthYear ]
df_master_data = df_master.select(*keep).na.fill(1600)

# Keep the playerID, yearID, AB, R, H, 2B, 3B, HR, RBI, SB, BB, HBP, SF from the Master Table

keep2 = ['playerID', 'yearID', 'AB', 'R', 'H', '2B', '3B', 'HR', 'RBI', 'SB', 'BB', 'HBP', 'SF']
df_bat_data = df_bat.select(*keep2)

# Replace null entries with Zero in the batting stats
df_bat_no_null = df_bat_data.na.fill(0)

# Filter out statistically insnificant entries for batting
df_bat_filt = df_bat_no_null.filter((df_bat_no_null.AB >= 502)).withColumnRenamed('2B', 'DB').withColumnRenamed('3B', 'TR')




# In[313]:

# Calculate Advanced batting stats, average, slugging pct, on base pct, on base plus slug pct

df_bat_stats = df_bat_filt.withColumn("AVG",  round(df_bat_filt.H/df_bat_filt.AB,3)).withColumn("SLG", \
round(((df_bat_filt.H -(df_bat_filt.DB + df_bat_filt.TR + df_bat_filt.HR))+ (2*df_bat_filt.DB) + \
(3*df_bat_filt.TR) + (4*df_bat_filt.HR)) /(df_bat_filt.AB),3)).withColumn("OBP", round((df_bat_filt.H + \
df_bat_filt.BB + df_bat_filt.HBP)/(df_bat_filt.AB + df_bat_filt.BB + df_bat_filt.HBP + \
df_bat_filt.SF),3))
df_bats_adv_stats = df_bat_stats.withColumn("OPS", round(df_bat_stats.OBP + df_bat_stats.SLG,3))


# Merge the two tables

cond = [df_master_data.playerID == df_bats_adv_stats.playerID]

# Join the Player Table and the Batting Table

df_bats_merge = df_bats_adv_stats.join(df_master_data, cond, 'inner')

# Calculate age of every player in the merged table

df_bats_merge_age_2 = df_bats_merge.withColumn("age", df_bats_merge.yearID - df_bats_merge.birthYear)
df_bats_merge_age = df_bats_merge_age_2.filter(df_bats_merge_age_2.age < 50)


#df_bats_merge_age.show()


# #### Pyspark Average Statistics
# To put our data into context, we can also look up the following information
#
# 1) Group players by their ages
#
#     a) Find the Average of the Batting Average for each age group
#     b) Find the Average of the On base Percentage for each age group
#     c) Find the Average of the Slugging Percentage for each age group
#     d) Find the Average of the On Base plus Slugging Percentage for each age group
#
#
#
#

# In[314]:

# Group all the players by their age, and calculate the average batting average for each age group



df_avg_stats = df_bats_merge_age.groupBy(df_bats_merge_age.age).agg({"AVG": "avg","SLG": "avg","OBP": "avg" , \
"OPS": "avg", "age": "count"}).orderBy(df_bats_merge_age.age)


#df_avg_stats.show()




# #### Pyspark Median and Quantile Statistics
# To put our data into furhter context, we can also look up the following information
#
# 1) Group players by their ages
#
#     a) Find the Median of the Batting Average for each age group
#     b) Find the Median of the On base Percentage for each age group
#     c) Find the Median of the Slugging Percentage for each age group
#     d) Find the Median of the On Base plus Slugging Percentage for each age group
#
# __NOTE: It appears Spark is not able to calculate Quantile information without HIVE. We did not install HIVE on our
#clusters, so instead we group players by age groups, and calculate a cumulative distribution for the batting average,
#slugging percentage, ob base percentage and On Base plus slugging percentage. This will allow us to divide players into
# quantiles. We use these quantiles to provide examples of how to look up the median data for a specific age group__
#
#

# In[315]:

# Calculating median data with HIVE is easy
df_bats_merge_age.registerTempTable("df")

df_med_stats_avg = sqlContext.sql("select age, percentile_approx(AVG,0.5) as medAvg from df group by age")
#df_med_stats_1 = df_med_stats.orderBy(df_med_stats.age)


df_med_stats_slg = sqlContext.sql("select age, percentile_approx(SLG,0.5) as medSlg from df group by age")
df_med_stats_avg_slg = df_med_stats_avg.join(df_med_stats_slg, ['age'])

df_med_stats_obp = sqlContext.sql("select age, percentile_approx(OBP,0.5) as medObp from df group by age")
df_med_stats_avg_slg_obp = df_med_stats_avg_slg.join(df_med_stats_obp, ['age'])

df_med_stats_ops = sqlContext.sql("select age, percentile_approx(OPS,0.5) as medOps from df group by age")
df_med_stats_avg_slg_obp_ops = df_med_stats_avg_slg_obp.join(df_med_stats_ops, ['age'])

keep3 = ['age', 'count(age)']
df_count_age = df_avg_stats.select(*keep3)

df_med_stats_avg_slg_obp_ops_age = df_med_stats_avg_slg_obp_ops.join(df_count_age, ['age']).orderBy(df_med_stats_avg_slg_obp_ops.age)


# Without Hive we have to improvise

#keep3 = ['yearID', 'AVG', 'SLG', 'OBP', 'OPS', 'age' ]
#df_filt_bat_data = df_bats_merge_age.select(*keep3)

#windowSpec = Window.partitionBy(df_filt_bat_data['age']).orderBy(df_filt_bat_data['AVG'].desc())
#windowSpec2 = Window.partitionBy(df_filt_bat_data['age']).orderBy(df_filt_bat_data['SLG'].desc())
#windowSpec3 = Window.partitionBy(df_filt_bat_data['age']).orderBy(df_filt_bat_data['OBP'].desc())
#windowSpec4 = Window.partitionBy(df_filt_bat_data['age']).orderBy(df_filt_bat_data['OPS'].desc())

#df_med_stats = df_filt_bat_data.withColumn("cumDistAvg", cume_dist().over(windowSpec)).withColumn("cumDistSlg",\
#cume_dist().over(windowSpec2)).withColumn("cumDistObp", cume_dist().over(windowSpec3)).withColumn("cumDistOps",\
#cume_dist().over(windowSpec4))

#df_med_stats.show()


# In[316]:

# Approx Median Batting Average for players of Age 27
# Answer: 0.283

#df_med_stats.filter(df_med_stats.age==27).filter("cumDistAvg> 0.495 AND cumDistAvg<0.505")


# In[317]:

# Approx Median OPS for players of Age 30
# Answer: 0.776

#df_med_stats.filter(df_med_stats.age==30).filter("cumDistOps> 0.495 AND cumDistOps<0.505")


# In[318]:

# Approx Median OBP for players of Age 25
# Answer: 0.334

#df_med_stats.filter(df_med_stats.age==25).filter("cumDistObp> 0.495 AND cumDistObp<0.505")


# In[319]:

# Approx Median SLG for players of Age 32, find the median Batting average
# Answer: 0.427

#df_med_stats.filter(df_med_stats.age==32).filter("cumDistSlg> 0.495 AND cumDistSlg<0.505")


# #### Pyspark Test Results
# We convert our spark data frames to pandas data frames, so it is easy to save them in a human readable csv format. \
#These files contain the answers to the questions we posed.

# In[335]:

# Examples to show how to print the results to an output file



pandas_bats_merge_age = df_bats_merge_age.toPandas()
pandas_avg_stats = df_avg_stats.toPandas()
pandas_med_stats = df_med_stats_avg_slg_obp_ops_age.toPandas()



print (time.time()-starttime)
