#Uncomment the line below when running in zeppelin notebooks.
#%spark2.pyspark

# Import the libraries we will need
import time
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count
from pyspark.sql.functions import avg
from pyspark.sql.functions import round
from pyspark.sql.functions import cume_dist
from pyspark.sql.window import Window

# Configure the authentication to our S3 instance
sc._jsc.hadoopConfiguration().set('fs.s3n.awsAccessKeyId', 'AKIAJYYXUAARX5BFPXRQ')
sc._jsc.hadoopConfiguration().set('fs.s3n.awsSecretAccessKey', '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')

# Load the data from our S3 instance
masterData = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Master.csv')
fieldingData = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Fielding.csv')

# Display the schema for both files
#masterData.printSchema()

# Start the benchmark timer
startTime = time.time()
print 'Starting benchmark at: {}'.format(startTime)

# Query the median weight and height for players by year

masterData = masterData.na.drop(subset=["height"])
masterData = masterData.na.drop(subset=["weight"])

# Merge the two data frames
fieldingData = fieldingData.join(masterData, masterData.playerID == fieldingData.playerID, 'inner')


# Query the median weight and height for players by year
fieldingData.createOrReplaceTempView('questionData')

# Generate our query
sqlDF = spark.sql('select yearID, avg(weight) as weight, avg(height) as height from questionData group by yearID order by yearID asc')

# Display results
#sqlDF.show()

# Export to CSV
pandassqlDF = sqlDF.toPandas()
#sqlDF.write.csv('/tmp/Question5.csv')

# End the benchmark timer
endTime = time.time()
print 'Ending benchmark at: {}'.format(endTime)
totalTime = endTime - startTime
print 'Total processing time: {}'.format(totalTime)