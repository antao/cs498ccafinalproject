# Uncomment the line below when running in Zeppelin notebooks
#%spark2.pyspark

import time

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count

# We create a sql context object, so that we can read in csv files easily, and create a data frame
sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", "AKIAJYYXUAARX5BFPXRQ")
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", "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 ")

df_master = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').\
load('s3n://cs498ccafinalproject/Master.csv')
df_field = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').\
load('s3n://cs498ccafinalproject/Fielding.csv')

starttime = time.time()

# #### Pyspark Data Operations to Determine how the Global Representation of Baseball players have changed from 1870 to 2016
#
# In order to determine how the Global representation of Major League Baseball players has changed over time, we perform the
# following operations
#
# 1) We perform an innner join on the Fielding.csv and Master.csv tables, using the playerID as a unique key.
#
# 2) We select only the columns that we need (playerID, birthCountry and yearID) to answer our question
#
# 3) We drop duplicate entries in the joined table. These can arise from players who played on multiple teams in the same
# year, or players who were called up to the majors, and dropped down to the minors multiple times a year.
#
# 4) We clean the database to remove any Null entries, for when the players country of orgin was unknown. This is
# especially common for the years between 1870 and 1912
#
# 5) We group the cleaned data by yearID and birthCountry, then perform an aggregation operation to determine the count.
#
# 6) We then sort the data by yearID
#
# This gives us a dataframe that lists the number of players born in a specific country, for every year from 1870 to 2016.

# In[96]:

# Join the two tables, and filter the colums we need.
# Remove duplicates
# Clean Null Entries
# Group by yearID and BirthCountry, then aggregate by Count
# Sort the final results by yearID


keep = [df_field.playerID, df_field.yearID, df_master.birthCountry ]
df_merge = df_field.join(df_master, df_field.playerID==df_master.playerID, 'inner').select(*keep).dropDuplicates()
df_clean = df_merge.filter(df_merge.birthCountry != "")
df_final = df_clean.groupBy(df_clean.yearID, df_clean.birthCountry).    agg(count("*")).    orderBy(df_clean.yearID)

#df_final.show()




# #### Pyspark Additional Statistics
# To put our data into context, we can also look up the following information
#
# 1) How many people have played in major league baseball from 1870 to 2016
#
# 2) How many unique countries have been represented by players in Major League Baseball from 1870 to 2016
#
# 3) How many people played Major League Baseball in the Year 2016
#
# 4) How many unique countries were represented by players in Major League Baseball from 1870 to 2016
#
#

# In[97]:

# Additional Examples showing how to get additional statistics
# Number of players in MLB from 1870 to 2016.
# Answer: 19105

df_master.count()


# In[98]:

# Additional Examples showing how to get additional statistics
# Number of Unique Countries that have had players in MLB from 1870 to 2016
# Answer: 53

df_clean.select(df_clean.birthCountry).distinct().count()


# In[99]:

# Additional Examples showing how to get additional statistics
# Number of MLB Players in 2016
# Answer: 1343

df_merge.filter(df_merge.yearID==2016).count()


# In[100]:

# Additional Examples showing how to get additional statistics
# Number of Countries represented in 2016
# Answer: 22
df_merge.filter(df_merge.yearID==2016).groupBy(df_merge.birthCountry).agg(count("*")).count()


# #### Pyspark Data Operations to show what Countries produce the most Major League Baseball players and which countries
# have shown the greatest increase and greatest decline in Major league players between 2001 and 2016.
#
# To determine which countries have produced the most baseball players in 2016, we slice the dataframe we obtained to
# determine global representation of players, for the year 2016. We can additionally look at a slice of this dataframe from
# 2001. If we join the two slices, and compute the differnce between players represented in 2016 and 2001, we can determine
# the corresponding percentage increase/decrease, as well as get a snapshot of which teams produce the most baseball players.
#
# From the Data it is obvious that USA produces the most players. It has 967 players in 2016 and 899 players in 2011. The
# Dominican Republic and Venezuela also had large representations with 134 and 102 players respectively.
#
# In terms of a statistically significant increase in players, Venezuela saw a 104% increase in players (50 to 102)
# represented from 2001 to 2016. Puerto Rico surprisingly showed a 51% decrease in players (53 to 26) represented from 2001
# to 2016.

# In[101]:

# Additional Examples showing how to get additional statistics
# Highest growth and Highest Decline in the Last 15 years
# Answer:
# Significant Increase - Venezuela (104%) from 50 to 102
# Significant Decrease - Puerto Rico (-51%) from 53 to 26
# Percentage Increase - Germany (300%) from 1 to 4. [Not Statistically significant]
# Percentage Decrease - Aruba (-67%) from 3 to 1. [Not Statistically significant]

df_2001 = df_final.filter(df_final.yearID==2001).withColumnRenamed('count(1)', 'countNum2001')
df_2016 = df_final.filter(df_final.yearID==2016).withColumnRenamed('count(1)', 'countNum2016')

df_ven_last_15 = df_final.filter(df_final.yearID>2000).withColumnRenamed('count(1)', 'countNum2001')

df_change = df_2016.join(df_2001, df_2016.birthCountry==df_2001.birthCountry, 'inner').    withColumn("diff", \
df_2016.countNum2016-df_2001.countNum2001)

df_perc_change = df_change.withColumn("percentChange", (df_change.diff/df_change.countNum2001)*100)

print (time.time()-starttime)