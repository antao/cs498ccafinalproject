# Uncomment the line below when running in Zeppelin notebooks
#%spark2.pyspark

# Import the libraries we will need
import time
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count
from pyspark.sql.functions import avg
from pyspark.sql.functions import round
from pyspark.sql.functions import cume_dist
from pyspark.sql.window import Window

# Configure the authentication to our S3 instance
sc._jsc.hadoopConfiguration().set('fs.s3n.awsAccessKeyId', 'AKIAJYYXUAARX5BFPXRQ')
sc._jsc.hadoopConfiguration().set('fs.s3n.awsSecretAccessKey', '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')

# Load the data from our S3 instance
masterData = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Master.csv')
pitchingData = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Pitching.csv')

# Display the schema for both files
#masterData.printSchema()
#pitchingData.printSchema()

# Start the benchmark timer
startTime = time.time()
#print 'Starting benchmark at: {}'.format(startTime)

# Merge the two data frames
questionData = pitchingData.join(masterData, masterData.playerID == pitchingData.playerID, 'left')

# Remove pitchers that played less than 7 games in a season
questionData = questionData.filter(questionData.G >= 7)

# Query the averge batting value for players by year
questionData = questionData.createOrReplaceTempView('questionData')

# Generate our query
sqlDF = spark.sql('select yearID, throws, avg(ERA) as ERA from questionData group by yearID, throws order by yearID asc')

# Remove NA, INF
sqlDF = sqlDF.na.drop(subset=["throws"])

# Display results
#sqlDF.show()

# Generate our query
sqlDF2 = spark.sql('select yearID, throws, avg(BAOpp) as BAOpp from questionData group by yearID, throws order by yearID asc')

# Remove NA, INF
sqlDF2 = sqlDF2.na.drop(subset=["throws"])

# Display results
sqlDF2 = sqlDF2.na.drop()

#sqlDF2.show()

pandas_sqlDF = sqlDF.toPandas()
pandas_sqlDF2 = sqlDF2.toPandas()

# Export to CSV
#pandas_sqlDF.to_csv('spark_question4_ERA_right_vs_lefty_pitchers.csv')
#pandas_sqlDF.to_csv('spark_question4_BAOpp_right_vs_lefty_pitchers')

# End the benchmark timer
endTime = time.time()
#print 'Ending benchmark at: {}'.format(endTime)
totalTime = endTime - startTime
print 'Total processing time: {}'.format(totalTime)