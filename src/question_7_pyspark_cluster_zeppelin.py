# Uncomment the line below when running in Zeppelin
#%spark2.pyspark

import time

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext

# We create a sql context object, so that we can read in csv files easily, and create a data frame
sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", "AKIAJYYXUAARX5BFPXRQ")
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", "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")

df_bat_post =sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/BattingPost.csv')
df_bat = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Batting.csv')
df_master = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('s3n://cs498ccafinalproject/Master.csv')


# 1) Only keep the playerID,  AB,  H
# 2) Replace null entries with Zero in the batting stats
keep = ['playerID', 'AB', 'H']
df_bat_post_data = df_bat_post.select(*keep).na.fill(0)
df_bat_data = df_bat.select(*keep).na.fill(0)
df_bat_data.join(df_bat_post_data,[df_bat_data.playerID == df_bat_post_data.playerID], 'inner')

# Sum the H and AB for each player
df_bat_post_data_agg = df_bat_post_data.groupBy(df_bat_post_data.playerID).agg({"H": "sum", "AB": "sum"})
df_bat_data_agg = df_bat_data.groupBy(df_bat_data.playerID).agg({"H": "sum", "AB": "sum"})

# Rename the collumns for easier use later
df_bat_post_data_agg = df_bat_post_data_agg.withColumnRenamed('sum(H)', 'sumH').withColumnRenamed('sum(AB)', 'sumAB')
df_bat_post_data_agg = df_bat_post_data_agg.filter(df_bat_post_data_agg.sumAB >= 60)
df_bat_post_stats = df_bat_post_data_agg.withColumn("PAVG", round(df_bat_post_data_agg.sumH/df_bat_post_data_agg.sumAB,3))

# Calculate the batting average for each player
df_bat_data_agg = df_bat_data_agg.withColumnRenamed('sum(H)', 'sumH').withColumnRenamed('sum(AB)', 'sumAB')
df_bat_data_agg = df_bat_data_agg.filter(df_bat_data_agg.sumAB >= 502)
df_bat_stats = df_bat_data_agg.withColumn("AVG", round(df_bat_data_agg.sumH/df_bat_data_agg.sumAB,3))

# Calcuate the batting difference between post and regular season
df_bat_diff = df_bat_post_stats.join(df_bat_stats,['playerID'],'inner')
df_bat_diff = df_bat_diff.withColumn("DIFF", round(df_bat_diff.PAVG  - df_bat_diff.AVG, 3))

#df_bat_diff.filter(df_bat_diff.playerID == 'soriaal01' ).show()

# Add firs and last name to list
keep = ['playerID', 'nameFirst', 'nameLast']
df_master = df_master.select(*keep)
df_bat_diff = df_bat_diff.join(df_master,['playerID'],'inner')

# Only show the stuff we care about
keep = ['playerID', 'nameFirst', 'nameLast', 'DIFF']
df_bat_diff = df_bat_diff.select(*keep)

# Display the values
df_bat_diff.orderBy(df_bat_diff['DIFF'].desc()).show()
df_bat_diff.orderBy(df_bat_diff['DIFF']).show()

# Conver to pandas for visulization

pandas_df = df_bat_diff.toPandas()