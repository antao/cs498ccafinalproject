
## Does money buy Championships? How have the Highest Spending Major League Baseball Teams performed over Time? 
____


In order to determine how the effect Team Salary expenditure has on Major League Baseball Team Performance, we look at Historical Baseball Data available on the Internet. The specific source of data chosen here is a database of baseball statistics over the years 1870 to 2016. http://www.seanlahman.com/baseball-database.html

This database has 27 tables. However to obtain the answer for our query above, we need to cross reference  data from 2 tables in this database. The Salaries.csv table lists every player that played in major league baseball, along with their team, and their associated salary. This data is only provided for the years 1985 and later. Its schema is listed below. 

#### Table 1: Salary Table Schema


| Field      | Description                            |
| ---------- | -------------------------------------- |
| yearID     | Year                                   |
| teamID     | Team                                   |
| lgID       | League                                 |
| playerID   | A unique code asssigned to each player |
| salary     | Player Salary                          |



*Note: At the Time of writing, the teamID in the Salaries.csv table for the year 2016 did not follow the convention of teamID's used throughout the rest of the table, and the entire database. Specifically 12 teams had teamIDs that did not match the code that had been used for their teamIDs in previous years. This data was manually cleaned to make sure it did not affect the Results obtained.*



The Teams.csv table lists the Team statistics for every team, that has played the game of baseball from 1870 to 2016, along with the year those statistics were recorded. Its schema is listed below

#### Table 2 Team Table schema


| Field          | Description                            |
| -------------- | -------------------------------------- |
| yearID         | Year                                   |
| lgID           | League                                 |
| teamID         | Team                                   |
| franchID       | Franchise                              |
| divID          | Teams Division                         |
| Rank           | Position in Final Standings            |
| G              | Games Played                           |
| GHome          | Games Played at Home                   |
| W              | Wins                                   |
| L              | Losses                                 |
| DivWin         | Division Winner                        |
| WCWin          | Wild Card Winner                       |
| LgWin          | League Champion                        |
| WSWin          | Word Series Champion                   |
| R              | Runs Scored                            |
| AB             | At Bats                                |
| H              | Hits                                   |
| 2B             | Doubles                                |
| 3B             | Triples                                |
| HR             | Homeruns                               |
| BB             | Batters Walked                         |
| SO             | Strike Outs                            |
| SB             | Stolen Bases                           |
| CS             | Caught Stealing                        |
| HBP            | Hit by Pitch                           |
| SF             | Sacrifice Flies                        |
| RA             | Opponent Runs Scored                   |
| ER             | Earned Runs Allowed                    |
| ERA            | Earned Run Average                     |
| CG             | Complete Games                         |
| SHO            | Shutouts                               |
| SV             | Saves                                  |
| IPOuts         | Outs Pitched                           |
| HA             | Hits Allowed                           |
| HRA            | Home Runs Allowed                      |
| BBA            | Walks Allowed                          |
| SOA            | Strikeouts by Pitchers                 |
| E              | Errors                                 |
| DP             | Double Plays                           |
| FP             | Fielding Percentage                    |
| name           | Teams Full Name                        |
| park           | Park                                   |
| attendance     | Home Attendance Total                  |
| BPF            | 3 Year Park Factor Batters             |
| PPF            | 3 Year Park Factor Pitchers            |
| teamIDBR       | Team ID Baseball Reference             |
| teamIDlahman45 | Team ID Baseball Reference Lahman 4.5  |
| teamIDretro    | Team ID Baseball Reference Retrosheet  |

We Utilize Apache Spark to perform the required database operations to answer our questions. The Code below explains the process of answering these questions, and shows how easy it is to use Spark to analyze Big Data. The Code to implement this query is implemented in Python, and can either be run on a local server or a cluster of servers. The example below was run on an Amazon EC2 Free Tier Ubuntu Server instance. The EC2 instance was set up  with Python (Anaconda 3-4.1.1),  Java, Scala, py4j, Spark and Hadoop. The code was written and executed in a Jupyter Notebook. Several guides are available on the internet describing how to install and run spark on an EC2 instance. One that particularly covers all these facets is https://medium.com/@josemarcialportilla/getting-spark-python-and-jupyter-notebook-running-on-amazon-ec2-dec599e1c297    

#### Pyspark Libraries
Import the pyspark libraries to allow python to interact with spark. A description of the basic functionality of each of these libaries is provided in the code comments below. A more detailed explanation of the functionality of each of these libraries can be found in Apache's documentation on Spark https://spark.apache.org/docs/latest/api/python/index.html


```python
# Import SparkContext. This is the main entry point for Spark functionality
# Import Sparkconf. We use Spark Conf to easily change the configuration settings when changing between local mode cluster mode. 
# Import SQLContext from pyspark.sql. We use the libraries here to read in data in csv format. The format of our native database
# Import count, sum, avg, rank from pyspark.sql.functions. This is used for the math operations needed to answer our questions
# Import Window from pyspark.sql to allow us to effectively partition and analyze data

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count
from pyspark.sql.functions import sum
from pyspark.sql.functions import rank
from pyspark.sql.functions import avg

from pyspark.sql.window import Window






```

#### Pyspark Configuration & Instantiation
We configure spark for local mode or cluster mode, configure our application name, and configure logging. Several other configuration settings can be programmed as well. A detailed explanation of these can be found at https://spark.apache.org/docs/latest/configuration.html

We pass the configuration to an instance of a SparkContext object, so that we can begin using Apache Spark


```python
# The Master will need to change when running on a cluster. 
# If we need to specify multiple cores we can list something like local[2] for 2 cores, or local[*] to use all available cores. 
# All the available Configuration settings can be found at https://spark.apache.org/docs/latest/configuration.html

sc_conf = SparkConf().setMaster('local[*]').setAppName('Question2').set('spark.logConf', True)
```


```python
# We instantiate a SparkContext object with the SparkConfig

sc = SparkContext(conf=sc_conf)

```

#### Pyspark CSV file Processing
We use the SQLContext library to easily allow us to read the csv files 'Salaries.csv' and 'Teams.csv'. These files are currently stored in Amazon s3 storage (s3://cs498ccafinalproject/) and are publicly available for download. They were copied over to a local EC2 instance by using the AWS command line interace command 

```aws s3 cp s3://cs498ccafinalproject . --recursive```


```python
# We create a sql context object, so that we can read in csv files easily, and create a data frame
sqlContext = SQLContext(sc)

df_salary = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('Salaries.csv')
df_team = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('Teams.csv')
```

#### Pyspark Data Operations to Determine the effect of Team Salary on Team Performance after 1984

In order to determine how the Global representation of Major League Baseball players has changed over time, we perform the following operations

1) We sum up the salary of every player, for every team, for every year. So we can obtain the yearly salary for each time

2) We perform an innner join on the Aggregated Salary data and the Teams.csv table, using the yearID and teamID as unique keys.

3) We select only the columns that we need (yearID, teamID, salary, W, DivWin, WCWin, LgWin, WSWin, R, H, HR, RA, ERA, HA    and HRA) to answer our question

4) We then sort the data by yearID and teamID. 

This gives us a dataframe that lists the every Team, for the years 1985 and beyond, and their corresponding Salary, Wins, Divisonal Rank, Division Champion state, Wild Card Winner state, League Champion state, World Series Champion state, Runs, Hits, Home Runs, Runs Allowed, Earned Run Average, Hits Allowed and Home Runs Alowed.    


```python
# Keep the year, team and salary data from the salary table
# Group the data by year and team, then sum up all the salaries
# Sort the data by year and then team

keep = [df_salary.yearID, df_salary.teamID, df_salary.salary ]
df_salary_filt = df_salary.select(*keep)

df_salary_agg_raw = df_salary_filt.groupBy(df_salary_filt.yearID, df_salary_filt.teamID).\
    agg(sum(df_salary_filt.salary)).\
    orderBy(df_salary_filt.yearID, df_salary_filt.teamID)

df_salary_agg = df_salary_agg_raw.withColumnRenamed('yearID', 'year').withColumnRenamed('teamID', 'team')


# From the Team table select the Wins, Divisonal Rank, Division Champion state, Wild Card Winner state, League Champion state,
# World Series Champion state, Runs, Hits, Home Runs, Runs Allowed, Earned Run Average, Hits Allowed and Home Runs Alowed

keep2 = [df_team.yearID, df_team.teamID, df_team.W, df_team.DivWin, df_team.WCWin, df_team.LgWin, df_team.WSWin, df_team.R, \
        df_team.H, df_team.HR, df_team.RA, df_team.ERA, df_team.HA, df_team.HRA]

df_team_filt = df_team.select(*keep2)

cond = [df_team_filt.yearID == df_salary_agg.year, df_team_filt.teamID == df_salary_agg.team]

# Join the Team Table and the Salart Table

df_merge_raw = df_team_filt.join(df_salary_agg, cond, 'inner')

df_merge = df_merge_raw.withColumnRenamed('sum(salary)', 'salary')

df_merge.show()
           

```

    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+--------+
    |yearID|teamID|  W|DivWin|WCWin|LgWin|WSWin|  R|   H| HR| RA| ERA|  HA|HRA|year|team|  salary|
    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+--------+
    |  1985|   ATL| 66|     N|     |    N|    N|632|1359|126|781|4.19|1512|134|1985| ATL|14807000|
    |  1985|   BAL| 83|     N|     |    N|    N|818|1451|214|764|4.38|1480|160|1985| BAL|11560712|
    |  1985|   BOS| 81|     N|     |    N|    N|800|1615|162|720|4.06|1487|130|1985| BOS|10897560|
    |  1985|   CAL| 90|     N|     |    N|    N|732|1364|153|703|3.91|1453|171|1985| CAL|14427894|
    |  1985|   CHA| 85|     N|     |    N|    N|736|1386|146|720|4.07|1411|161|1985| CHA| 9846178|
    |  1985|   CHN| 77|     N|     |    N|    N|686|1397|150|729|4.16|1492|156|1985| CHN|12702917|
    |  1985|   CIN| 89|     N|     |    N|    N|677|1385|114|666|3.71|1347|131|1985| CIN| 8359917|
    |  1985|   CLE| 60|     N|     |    N|    N|729|1465|116|861|4.91|1556|170|1985| CLE| 6551666|
    |  1985|   DET| 84|     N|     |    N|    N|729|1413|202|688|3.78|1313|141|1985| DET|10348143|
    |  1985|   HOU| 83|     N|     |    N|    N|706|1457|121|691|3.66|1393|119|1985| HOU| 9993051|
    |  1985|   KCA| 91|     Y|     |    Y|    Y|687|1384|154|639|3.49|1433|103|1985| KCA| 9321179|
    |  1985|   LAN| 95|     Y|     |    N|    N|682|1434|129|579|2.96|1280|102|1985| LAN|10967917|
    |  1985|   MIN| 77|     N|     |    N|    N|705|1453|141|782|4.48|1468|164|1985| MIN| 5764821|
    |  1985|   ML4| 71|     N|     |    N|    N|690|1467|101|802|4.39|1510|175|1985| ML4|11284107|
    |  1985|   MON| 84|     N|     |    N|    N|633|1342|118|636|3.55|1346| 99|1985| MON| 9470166|
    |  1985|   NYA| 97|     N|     |    N|    N|839|1458|176|660|3.69|1373|157|1985| NYA|14238204|
    |  1985|   NYN| 98|     N|     |    N|    N|695|1425|134|568|3.11|1306|111|1985| NYN|10834762|
    |  1985|   OAK| 77|     N|     |    N|    N|757|1475|155|787|4.41|1451|172|1985| OAK| 9058606|
    |  1985|   PHI| 75|     N|     |    N|    N|667|1343|141|673|3.68|1424|115|1985| PHI|10124966|
    |  1985|   PIT| 57|     N|     |    N|    N|568|1340| 80|708|3.97|1406|107|1985| PIT| 9227500|
    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+--------+
    only showing top 20 rows
    


#### Pyspark Additional Statistics
To put our data into context, we can also look up the following information

1) Extract the Highest spending teams each year (1984 and later), and examine their performance

    a) Find the Number and Percentage of times the Top Spending Team has won the World Series 
    b) Find the Number and Percentage of times the Top Spending Team has won their League (AL/NL)
    c) Find the Number and Percentage of times the Top Spending Team has won their Division 
    d) Find the Number and Percentage of times the Top Spending Team has made the Playoffs
    
2) Extract the World Series winning team each year, and examine their salary rank

    a) Find the Number and Percentage of times the Word Series Winner is in the Top 5 spenders in the league
    b) Find the Number and Percentage of times the Word Series Winner is in the Top 10 spenders in the league




```python
# Rank each team by the amount of money spent every year

windowSpec = Window.partitionBy(df_merge['yearID']).orderBy(df_merge['salary'].desc()) 
df_big_spender = df_merge.withColumn("yearRank", rank().over(windowSpec))

   
```


```python
# Extract the Top Spending Team Every Year, and look at their performance

df_top_spender = df_big_spender.filter(df_big_spender.yearRank == 1).orderBy(df_big_spender['yearID'], ascending=False)
df_top_spender.show()
```

    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+---------+--------+
    |yearID|teamID|  W|DivWin|WCWin|LgWin|WSWin|  R|   H| HR| RA| ERA|  HA|HRA|year|team|   salary|yearRank|
    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+---------+--------+
    |  2016|   NYA| 84|     N|    N|    N|    N|680|1378|183|702|4.16|1358|214|2016| NYA|222997792|       1|
    |  2015|   LAN| 92|     Y|    N|    N|    N|667|1346|187|595|3.44|1317|145|2015| LAN|215792000|       1|
    |  2014|   LAN| 94|     Y|    N|    N|    N|718|1476|134|617| 3.4|1338|142|2014| LAN|217014600|       1|
    |  2013|   NYA| 85|     N|    N|    N|    N|650|1321|144|671|3.94|1452|171|2013| NYA|231978886|       1|
    |  2012|   NYA| 95|     Y|    N|    N|    N|804|1462|245|668|3.84|1401|190|2012| NYA|196522289|       1|
    |  2011|   NYA| 97|     Y|    N|    N|    N|867|1452|222|657|3.73|1423|152|2011| NYA|202275028|       1|
    |  2010|   NYA| 95|     N|    Y|    N|    N|859|1485|201|693|4.06|1349|179|2010| NYA|206333389|       1|
    |  2009|   NYA|103|     Y|    N|    Y|    Y|915|1604|244|753|4.26|1386|181|2009| NYA|201449189|       1|
    |  2008|   NYA| 89|     N|    N|    N|    N|789|1512|180|727|4.28|1478|143|2008| NYA|207896789|       1|
    |  2007|   NYA| 94|     N|    Y|    N|    N|968|1656|201|777|4.49|1498|150|2007| NYA|189259045|       1|
    |  2006|   NYA| 97|     Y|    N|    N|    N|930|1608|210|767|4.41|1463|170|2006| NYA|194663079|       1|
    |  2005|   NYA| 95|     Y|    N|    N|    N|886|1552|229|789|4.52|1495|164|2005| NYA|208306817|       1|
    |  2004|   NYA|101|     Y|    N|    N|    N|897|1483|242|808|4.69|1532|182|2004| NYA|184193950|       1|
    |  2003|   NYA|101|     Y|    N|    Y|    N|877|1518|230|716|4.02|1512|145|2003| NYA|152749814|       1|
    |  2002|   NYA|103|     Y|    N|    N|    N|897|1540|223|697|3.87|1441|144|2002| NYA|125928583|       1|
    |  2001|   NYA| 95|     Y|    N|    Y|    N|804|1488|203|713|4.02|1429|158|2001| NYA|112287143|       1|
    |  2000|   NYA| 87|     Y|    N|    Y|    Y|871|1541|205|814|4.76|1458|177|2000| NYA| 92338260|       1|
    |  1999|   NYA| 98|     Y|    N|    Y|    Y|900|1568|193|731|4.13|1402|158|1999| NYA| 86734359|       1|
    |  1998|   BAL| 79|     N|    N|    N|    N|817|1520|214|785|4.73|1505|169|1998| BAL| 72355634|       1|
    |  1997|   NYA| 96|     N|    Y|    N|    N|891|1636|161|688|3.84|1463|144|1997| NYA| 62241545|       1|
    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+---------+--------+
    only showing top 20 rows
    



```python
# Number of time the Top Spending Team has won the world series after 1984
# Ans: 5

topSpendWs = df_top_spender.filter(df_top_spender.WSWin=="Y").count()
teamCount = df_top_spender.count()

print (topSpendWs)
```

    5



```python
# Percentage of time the top spending team has won the world series after 1984
# Ans: 15.635 %

print ((topSpendWs/teamCount)*100)
```

    15.625



```python
# Number of times the top spending team has won their League (AL/NL) after 1984
# Ans: 7

topSpendLg = df_top_spender.filter(df_top_spender.LgWin=="Y").count()
print (topSpendLg)
```

    7



```python
# Percentage of time the top spending team has won their League (AL/NL) after 1984
# Ans: 21.875 %

print ((topSpendLg/teamCount)*100)

```

    21.875



```python
# Number of times the top spending team has won their Division after 1984
# Ans: 15

topSpendDiv = df_top_spender.filter(df_top_spender.DivWin=="Y").count()
print (topSpendDiv)
```

    15



```python
# Percentage of time the top spending team has won their Division after 1984
# Ans: 46.875%

print ((topSpendDiv/teamCount)*100)
```

    46.875



```python
# Number of times the top spending team has made it to the Playoffs
# Ans: 19

topSpendPl = df_top_spender.filter("DivWin=='Y' OR WCWin=='Y'").count()
```


```python
# Percentage of times the top spending team has made it to the Playoffs
# Ans: 59.375%

print((topSpendPl/teamCount)*100)
```

    59.375



```python
# Extract the Word Series Team Every Year, and look at their Spending Rank

df_ws_winner = df_big_spender.filter(df_big_spender.WSWin == "Y").orderBy(df_big_spender['yearID'], ascending=False)
df_ws_winner.show()


```

    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+---------+--------+
    |yearID|teamID|  W|DivWin|WCWin|LgWin|WSWin|  R|   H| HR| RA| ERA|  HA|HRA|year|team|   salary|yearRank|
    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+---------+--------+
    |  2016|   CHN|103|     Y|    N|    Y|    Y|808|1409|199|556|3.15|1125|163|2016| CHN|154067668|       8|
    |  2015|   KCA| 95|     Y|    N|    Y|    Y|724|1497|139|641|3.73|1372|155|2015| KCA|112107025|      17|
    |  2014|   SFN| 88|     N|    Y|    Y|    Y|665|1407|132|614| 3.5|1305|133|2014| SFN|163510167|       4|
    |  2013|   BOS| 97|     Y|    N|    Y|    Y|853|1566|178|656|3.79|1366|156|2013| BOS|151530000|       4|
    |  2012|   SFN| 94|     Y|    N|    Y|    Y|718|1495|103|649|3.68|1361|142|2012| SFN|117620683|       8|
    |  2011|   SLN| 90|     N|    Y|    Y|    Y|762|1513|162|692|3.74|1461|136|2011| SLN|105433572|      11|
    |  2010|   SFN| 92|     Y|    N|    Y|    Y|697|1411|162|583|3.36|1279|134|2010| SFN| 98641333|       9|
    |  2009|   NYA|103|     Y|    N|    Y|    Y|915|1604|244|753|4.26|1386|181|2009| NYA|201449189|       1|
    |  2008|   PHI| 92|     Y|    N|    Y|    Y|799|1407|214|680|3.88|1444|160|2008| PHI| 97879880|      12|
    |  2007|   BOS| 96|     Y|    N|    Y|    Y|867|1561|166|657|3.87|1350|151|2007| BOS|143026214|       2|
    |  2006|   SLN| 83|     Y|    N|    Y|    Y|781|1484|184|762|4.54|1475|193|2006| SLN| 88891371|      10|
    |  2005|   CHA| 99|     Y|    N|    Y|    Y|741|1450|200|645|3.61|1392|167|2005| CHA| 75178000|      13|
    |  2004|   BOS| 98|     N|    Y|    Y|    Y|949|1613|222|768|4.18|1430|159|2004| BOS|127298500|       2|
    |  2003|   FLO| 91|     N|    Y|    Y|    Y|751|1459|157|692|4.04|1415|128|2003| FLO| 49450000|      24|
    |  2002|   ANA| 99|     N|    Y|    Y|    Y|851|1603|152|644|3.69|1345|169|2002| ANA| 61721667|      15|
    |  2001|   ARI| 92|     Y|    N|    Y|    Y|818|1494|208|677|3.87|1352|195|2001| ARI| 85082999|       8|
    |  2000|   NYA| 87|     Y|    N|    Y|    Y|871|1541|205|814|4.76|1458|177|2000| NYA| 92338260|       1|
    |  1999|   NYA| 98|     Y|    N|    Y|    Y|900|1568|193|731|4.13|1402|158|1999| NYA| 86734359|       1|
    |  1998|   NYA|114|     Y|    N|    Y|    Y|965|1625|207|656|3.82|1357|156|1998| NYA| 66806867|       2|
    |  1997|   FLO| 92|     N|    Y|    Y|    Y|740|1410|136|669|3.83|1353|131|1997| FLO| 48692500|       8|
    +------+------+---+------+-----+-----+-----+---+----+---+---+----+----+---+----+----+---------+--------+
    only showing top 20 rows
    



```python
# Number of times the World Series Winner is in the Top 5 spenders in the League (After 1984)
# Ans: 14

wsTop5 = df_ws_winner.filter(df_ws_winner.yearRank <=5).count()
teamCount = df_ws_winner.count()
print (wsTop5)
```

    14



```python
# Percentage of times the World Series Winner is in the Top 5 spenders in the League (After 1984)
# Ans: 45.16%

print ((wsTop5/teamCount)*100)
```

    45.16129032258064



```python
# Number of times the World Series Winner is in the Top 10 spenders in the League (After 1984)
# Ans: 21

dvTop5 = df_ws_winner.filter(df_ws_winner.yearRank <=10).count()
print (dvTop5)
```

    21



```python
# Percentage of times the World Series Winner is in the Top 10 spenders in the League (After 1984)
# Ans: 67.74%

print ((dvTop5/teamCount)*100)
```

    67.74193548387096


#### Calculate the Average Number of Wins and Average Salary of the Top Spending Teams
We can partition the data by a Teams spending rank, and look at the Average number of wins achieved by each team based on their spending rank. We can also look at their Average amount of Salary expenditure. From the Data we see that the Teams that spend the most on average, also win the most on average. The Highest spending teams since 1984 have averaged 89 wins per season. They have also roughly spent an average of $118 million on salary.

There is a large gap between the Average spending of the Top team, and the Average spending of the Team that spends the fifth most amount of money. There is roughly a 50 percent increase in average spending between these teams. However there is only an average difference of 5 wins per season between these teams.  



```python
# Calculate the Average number of wins by Spending Rank

windowSpec2 = Window.partitionBy(df_big_spender['yearRank']).orderBy(df_big_spender['yearRank'].desc())
df_avg_win = df_big_spender.withColumn("avgWin", avg(df_big_spender['W']).over(windowSpec2)).\
    withColumn("avgSal", 0.000001*avg(df_big_spender['salary']).over(windowSpec2))
    

df_avg_win_fin = df_avg_win.filter(df_avg_win.yearID == 2016).orderBy(df_avg_win['yearRank'])

keep4 = [df_avg_win_fin.yearRank, df_avg_win_fin.avgWin, df_avg_win_fin.avgSal]
df_avg_win_fin_filt = df_avg_win_fin.select(*keep4)

df_avg_win_fin_filt.show()


```

    +--------+--------+------------------+
    |yearRank|  avgWin|            avgSal|
    +--------+--------+------------------+
    |       1|89.03125|   118.16011821875|
    |       2| 85.5625|       99.23958725|
    |       3| 83.4375| 89.63457199999999|
    |       4|86.34375|    84.31810384375|
    |       5| 84.5625|    80.05574271875|
    |       6|85.21875| 77.29115903124999|
    |       7| 81.5625|     73.5204090625|
    |       8|85.65625|    70.29353221875|
    |       9|  81.625|       67.92195625|
    |      10|   81.75|     65.3522938125|
    |      11|82.09375|       64.33172175|
    |      12|80.78125|62.649797718749994|
    |      13|80.40625|      60.650399125|
    |      14|79.96875|     58.0998454375|
    |      15|80.09375|    56.18513246875|
    |      16|80.90625|53.977201843749995|
    |      17|   81.25|      52.276651625|
    |      18|77.71875|     50.5717759375|
    |      19|78.34375|48.743505218749995|
    |      20| 74.6875|    47.01839896875|
    +--------+--------+------------------+
    only showing top 20 rows
    


#### Pyspark Test Results
We convert our spark data frames to pandas data frames, so it is easy to save them in a human readable csv format. These files contain the answers to the questions we posed.


```python
# Examples to show how to print the results to an output file

keep3 = [df_merge.yearID, df_merge.teamID, df_merge.W, df_merge.salary]
df_merge_wins_sal = df_merge.select(*keep3).filter(df_merge.yearID>2006)

pandas_merge_wins_sal = df_merge_wins_sal.toPandas()

pandas_merge = df_merge.toPandas()
pandas_top_spender = df_top_spender.toPandas()
pandas_ws_winner = df_ws_winner.toPandas()
pandas_avg_sal_wins = df_avg_win_fin_filt.toPandas()

pandas_merge_wins_sal.to_csv('spark_question2_wins_salary_last_10.csv')
pandas_merge.to_csv('spark_question2_salary.csv')
pandas_top_spender.to_csv('spark_question2_top_spender.csv')
pandas_ws_winner.to_csv('spark_question2_ws_winner.csv')
pandas_avg_sal_wins.to_csv('spark_question2_avg_sal_wins.csv')
```


```python
sc.stop()
```
