### Which U.S States have produced the Most Baseball Players?


##### Visualizing the Data

We were able to use Apache Spark to extract the birthplaces of players born in the United States and summarize
the locations that have produced the most major league baseball players.

##### Reading the data

```{r echo = FALSE}
# Clear the environment
rm(list=ls())

# Load the necessary libraries, using pacman as a package manager
if (!require("pacman")) install.packages("pacman")
pacman::p_load(sqldf, highcharter, htmlwidgets, webshot, RCurl, jsonlite)
```

The data was originally generated through our Spark instance and saved in a comma delimited file.  We will
begin the visualization by reading the summary from the comma delimited file.

```{r}
# Read the data from the CSV
question6Data <- read.csv(file = 'spark_question6_US_State_Rep.csv')

```

The visualizations are generated using the highcharter package which generates highcharts visualizations
in R.   Highcharts (https://www.highcharts.com/) is based on D3.js (https://d3js.org/).

```{r}
chart6 <- hcmap('countries/us/us-all', data = question6Data, value = 'myCount',
      joinBy = c('hc-a2', 'birthState'), name = 'US Player Birth State',
      dataLabels = list(enabled = TRUE),
      borderColor = '#FAFAFA', borderWidth = 0.1)
```

```{r}
# Display the visualization
chart6
```
