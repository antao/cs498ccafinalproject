
## Which U.S States have produced the Most Baseball Players?
____


In order to determine which U.S States have produced the most Major League Baseball players, we look at Historical Baseball Data available on the Internet. The specific source of data chosen here is a database of baseball statistics over the years 1870 to 2016. http://www.seanlahman.com/baseball-database.html


This database has 27 tables. However to obtain the answer for our query above, we need to cross reference  data from 2 tables in this database. The Master.csv table lists every player that has played the game from 1870 to 2016, along with their year of birth . Its schema is listed below. 

#### Table 1: Master Table Schema


| Field      | Description                            |
| ---------- | -------------------------------------- |
| playerID   | A unique code asssigned to each player |
| birthYear  | Year player was born                   |
| birthMonth | Month player was born                  |
| birthDay   | Day player was born                    |
| birthCount | Country where player was born          |
| birthState | State where player was born            |
| birthCity  | City where player was born             |
| deathYear  | Year player died                       |
| deathMonth | Month player died                      |
| deathDay   | Day player died                        |
| deathCount | Country where player died              |
| deathState | State where player died                |
| deathCity  | City where player died                 |
| nameFirst  | Player's first name                    |
| nameLast   | Player's last name                     |
| nameGiven  | Player's given name                    |
| weight     | Player's weight in pounds              |
| height     | Player's height in inches              |
| bats       | Player's batting hand (left, right)    |
| throws     | Player's throwing hand (left or right) |
| debut      | Date that player made first appearance |
| finalGame  | Date that player made last appearance  |
| retroID    | ID used by retrosheet                  |
| bbrefID    | ID used by Baseball Reference website  |



We Utilize Apache Spark to perform the required database operations to answer our questions. The Code below explains the process of answering these questions, and shows how easy it is to use Spark to analyze Big Data. The Code to implement this query is implemented in Python, and can either be run on a local server or a cluster of servers. The example below was run on an Amazon EC2 Free Tier Ubuntu Server instance. The EC2 instance was set up  with Python (Anaconda 3-4.1.1),  Java, Scala, py4j, Spark and Hadoop. The code was written and executed in a Jupyter Notebook. Several guides are available on the internet describing how to install and run spark on an EC2 instance. One that particularly covers all these facets is https://medium.com/@josemarcialportilla/getting-spark-python-and-jupyter-notebook-running-on-amazon-ec2-dec599e1c297    

#### Pyspark Libraries
Import the pyspark libraries to allow python to interact with spark. A description of the basic functionality of each of these libaries is provided in the code comments below. A more detailed explanation of the functionality of each of these libraries can be found in Apache's documentation on Spark https://spark.apache.org/docs/latest/api/python/index.html


```python
# Import SparkContext. This is the main entry point for Spark functionality
# Import Sparkconf. We use Spark Conf to easily change the configuration settings when changing between local mode cluster mode. 
# Import SQLContext from pyspark.sql. We use the libraries here to read in data in csv format. The format of our native database
# Import count, avg, round from pyspark.sql.functions. This is used for the math operations needed to answer our questions
# Import Window from pyspark.sql to allow us to effectively partition and analyze data

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import count
from pyspark.sql.functions import avg
from pyspark.sql.functions import round
from pyspark.sql.functions import cume_dist


from pyspark.sql.window import Window



```

#### Pyspark Configuration & Instantiation
We configure spark for local mode or cluster mode, configure our application name, and configure logging. Several other configuration settings can be programmed as well. A detailed explanation of these can be found at https://spark.apache.org/docs/latest/configuration.html

We pass the configuration to an instance of a SparkContext object, so that we can begin using Apache Spark


```python
# The Master will need to change when running on a cluster. 
# If we need to specify multiple cores we can list something like local[2] for 2 cores, or local[*] to use all available cores. 
# All the available Configuration settings can be found at https://spark.apache.org/docs/latest/configuration.html

sc_conf = SparkConf().setMaster('local[*]').setAppName('Question6').set('spark.logConf', True)
```


```python
# We instantiate a SparkContext object with the SparkConfig

sc = SparkContext(conf=sc_conf)

```

#### Pyspark CSV file Processing
We use the SQLContext library to easily allow us to read the csv files 'Salaries.csv' and 'Teams.csv'. These files are currently stored in Amazon s3 storage (s3://cs498ccafinalproject/) and are publicly available for download. They were copied over to a local EC2 instance by using the AWS command line interace command 

```aws s3 cp s3://cs498ccafinalproject . --recursive```


```python
# We create a sql context object, so that we can read in csv files easily, and create a data frame
sqlContext = SQLContext(sc)

masterData = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('Master.csv')


```

#### Pyspark Data Operations. 

To figure out how many players have come from each U.S State, we perform the following operations

1) We perform a query to first filter players belonging to the United States, then group the Players by their birth state, and returning the count of players from each state 

2) We sort the Data by State

The data shows that California has produced the most players (2193) . Pennsylvania (1415), Neywork (1216) , Illinois (1057) and Ohio (1041) have produced a significant number of players as well. Alaska (12) , Wyoming (16) and North Dakota (17) have produced the least.  




```python


# Query the birth state and count for all US born players
questionData = masterData.createOrReplaceTempView('masterData')

# Generate our query
sqlDF = sqlContext.sql('select birthState, count(birthState) as myCount from masterData where birthCountry = "USA" group by birthState order by birthState asc')

# Display results
sqlDF.show()



         

```

    +----------+-------+
    |birthState|myCount|
    +----------+-------+
    |        AK|     12|
    |        AL|    329|
    |        AR|    153|
    |        AZ|    101|
    |        CA|   2193|
    |        CO|     92|
    |        CT|    206|
    |        DC|    102|
    |        DE|     56|
    |        FL|    520|
    |        GA|    352|
    |        HI|     41|
    |        IA|    221|
    |        ID|     30|
    |        IL|   1057|
    |        IN|    376|
    |        KS|    212|
    |        KY|    283|
    |        LA|    251|
    |        MA|    667|
    +----------+-------+
    only showing top 20 rows
    
    

#### Pyspark Test Results
We convert our spark data frames to pandas data frames, so it is easy to save them in a human readable csv format. These files contain the answers to the questions we posed.


```python

# Export to CSV
pandassqlDF = sqlDF.toPandas()
pandassqlDF.to_csv('spark_question6_US_State_Rep.csv')

```


```python
sc.stop()
```
